from random import randint

print("Guess birthday of 1-50")


mlow = 1
mhigh = 12

ylow = 1924
yhigh = 2004

for guess_number in range(5):
    guess_month = randint(mlow, mhigh)
    guess_year = randint(ylow, yhigh)

    print("I guessed", guess_month, "/", guess_year)

    response = input("its your number, equal, higher, or lower?")

    if response == 'equal':
        print("I (the computer) guessd it")
        break

    elif response == 'higher':
        print("my guess was too low")
        ylow = guess_year +1

    else:
        print("My guess was too high")
        yhigh = guess_year - 1
